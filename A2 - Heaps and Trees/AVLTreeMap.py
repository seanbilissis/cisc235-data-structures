import DisplayTree as dt

class AVLNode:
    ''' AVLTreeMap node'''
    def __init__(self, key = None, value = None):
        self.key = key
        self.value = value
        self.left = None
        self.right = None
        self.height = 1


class AVLTreeMap:
    '''AVLTreeMap object'''
    def __init__(self):
        self.root = None

    def getHeight(self, node):
        '''Function that takes a node as input and returns it's height'''
        if node == None:
        #     return -1
        # elif node.right == None and node.left == None:  # leaf
            return 0
        else: 
            return node.height

    def getBalanceFactor(self, node):
        '''Function that determines the balance factor for a given node'''
        # check if empty node - return 0 if so
        if node == None: 
            return 0
        # node isn't empty (not leaf) - determine balance factor 
        # by taking the difference between height of left and right subtrees
        return self.getHeight(node.right) - self.getHeight(node.left)

    def get(self, key):
        ''' Search tree for given key and return associated value if found, None otherwise '''
        # start at root
        node = self.root
        # continue search until end of tree is reached (or match found) 
        while node != None: 
            # found a matching node - return its value
            if key == node.key: 
                return node.value
            # search key is creater than current node - look in right subtree
            elif key > node.key:
                node = node.right
            # search key is less than current node - look in left subtree
            else: 
                node = node.left
        # No match found - return None
        return None
    
    def recursivePut(self, node, key, value):
        '''Recursive function for inserting a node with key-value pair into AVLTreeMap 
        and adjusts the positions of nodes if the tree needs balancing'''
        # No node in the current position -- insert new node here
        if node == None:
            return AVLNode(key, value)
        # key is less than the key of node in the current position - insert somewhere in the left subtree
        elif key < node.key:
            node.left = self.recursivePut(node.left, key, value)
        # key is greater than the key of node in the current position - insert somewhere in the right subtree
        else:
            node.right = self.recursivePut(node.right, key, value)
        
        # Update height of ancestor node
        node.height = max(self.getHeight(node.left), self.getHeight(node.right)) + 1

        # Get balance factor
        balanceFactor = self.getBalanceFactor(node)

        # Rebalance if one of the following cases apply:
        # Left Left - right rotation to balance
        if balanceFactor < -1 and key < node.left.key:
            return self.rightRotate(node)
        # Right Right - left rotation to balance
        if balanceFactor > 1 and key > node.right.key:
            return self.leftRotate(node)
        # Left Right - left rotation followed by right rotation to balance
        if balanceFactor < -1 and key > node.left.key:
            node.left = self.leftRotate(node.left)
            return self.rightRotate(node)
        # Right Left - right roatation followed by left rotation to balance
        if balanceFactor > 1 and key < node.right.key:
            node.right = self.rightRotate(node.right)
            return self.leftRotate(node)

        # node is balanced and can be returned
        return node

    def put(self, key, value):
        '''Wrapper for recursive function "recursivePut" - called to insert a new node into the tree'''
        # call recursivePut with root as starting node
        self.root = self.recursivePut(self.root, key, value)

    def leftRotate(self, x):
        ''' Performs Left Rotation of node and updates the height of effected nodes '''
        # save nodes into temp variables for swapping
        y = x.right
        z = y.left
        # perform left rotation 
        y.left = x
        x.right = z
        # update heights of nodes after adjustments to their positions are made
        x.height = max(self.getHeight(x.left), self.getHeight(x.right)) + 1
        y.height = max(self.getHeight(y.left), self.getHeight(y.right)) + 1

        return y

    def rightRotate(self, x):
        ''' Performs Right Rotation of node and updates the height of effected nodes'''
        # save nodes into temp variables for swapping
        y = x.left
        z = y.right
        # perform right rotation
        y.right = x
        x.left = z
        # update heights of nodes after adjustments to their positions are made
        x.height = max(self.getHeight(x.left), self.getHeight(x.right)) + 1
        y.height = max(self.getHeight(y.left), self.getHeight(y.right)) + 1

        return y 

if __name__ == "__main__":
    
    # Initialize Tree
    AVL = AVLTreeMap()

    # Insert key-value pairs
    AVL.put(15, "bob") 
    AVL.put(20, "anna") 
    AVL.put(24, "tom") 
    AVL.put(10, "david") 
    AVL.put(13, "david") 
    AVL.put(7, "ben") 
    AVL.put(30, "karen") 
    AVL.put(36, "erin") 
    AVL.put(25, "david")

    # Display AVL
    dt.print2D(AVL.root)

    # Test get function by extracting key-value pairs
    t1 = AVL.get(50) == None
    t2 = AVL.get(20) == "anna" 
    t3 = AVL.get(13) == "david"
    t4 = AVL.get(24) == "tom"
    t5 = AVL.get(30) == "karen"
    t6 = AVL.get(25) == "david"
    t7 = AVL.get(12) == None
    
    # True if all tests above were successful
    testall = t1 and t2 and t3 and t4 and t5 and t6 and t7 
    print(testall)

