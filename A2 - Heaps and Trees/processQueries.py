import os
import WebPageIndex as WPI
import WebpagePriorityQueue as WPQ

def readFiles(dir):
    '''Function that takes a directory path, saves the files in the directory
    to a list, then creates a WebPageIndex instance for each file and returns a 
    list of the instances'''
    wpi_list = []   # initialize list of instances
    path_list = os.listdir(dir)    # extract path to files in director to a list
    
    # iterate through file paths in the directory - create a WPI instance for each
    for path in path_list:
        prefix = "data\\" 
        wpi_list.append(WPI.WebPageIndex(prefix+path))
    
    return wpi_list

def main(queryFile, limit = None):
    '''main/driver function - takes a path to a file containing queries, creates
    a WebPriorityQueue object, and reheaps the priority queue for each query - printing
    the highest priority webpages in order'''

    wpi_list = readFiles("data") # get WPI instances
    priority_queue = WPQ.WebpagePriorityQueue(wpi_list)     # create WebpagePriorityQueue instance with WPI instances

    # open and read each line of the query file - save each query to list
    with open(queryFile, "r") as f:
        query_list = f.readlines()
    # iterate through search queries - reheaping the priority queue for each
    for query in query_list:
        print("Results for:  " + query)
        priority_queue.reheap(query)

        c = 0           # result counter
        flag = False    # flag to indicate when loop should terminate
        while not flag:
            x = priority_queue.peek() # peak top of heap
            # if there was a search result, get instance of webpage
            if x.priority > 0:
                print(priority_queue.poll().instance.path)
                c += 1
                # check if limit is reached - terminate if so
                if limit != None and c >= limit:
                    flag = True
            # no occurences of query in any other webpages - stop loop
            else:
                flag = True
            

if __name__=="__main__":
    
    query_path = "queries.txt"
    print("Specify maximum number of webpage matches (press <enter> for no limit): ")
    limit = input()

    if not limit:
        main(query_path)
    else:
        main(query_path, int(limit))
