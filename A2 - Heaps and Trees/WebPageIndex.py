import DisplayTree as dt

class WPINode:
    '''WebPageIndex Node'''
    def __init__(self, key = None, value = None):
        self.key = key
        self.value = []
        self.left = None
        self.right = None
        self.height = 1

        # append value to the value attribute list
        self.value.append(value)

class WebPageIndex(): 
    '''WebPageIndex Object'''
    def __init__(self, file):
        self.path = file
        self.root = None 

        # Initialize by extracting words from the file path in path attribute
        with open(self.path, "r") as f:     # open file for reading
            doc = f.read().lower()          # read contents, change to lowercase, and store as string
            words = doc.split()             # seperate the string containing the contents by whitespace - save into list
            words = [word.strip('.,;:!?()[]') for word in words]    # remove punctuation/special characters
        
        end = len(words) 
        i = 0
        # Insert a node for every word in the list of words, along with its index
        while i < end:
            self.put(words[i], i)   # insert node with "put" function
            i += 1                  # increment loop condition
        
    def getCount(self, s):
        ''' Returns the number of occurences of the string s in the input file '''
        node = self.root
        # search for the node with s as a key
        while node != None: 
            # found a match - return length of the nodes value attribute (stores index of all occurences)
            if s == node.key:
                return len(node.value)
            # s is greater than current nodes key - search in right subtree
            elif s > node.key:
                node = node.right
            # s is less than current nodes key - search in left subtree
            else: 
                node = node.left
        # no match found - return 0
        return 0
    
    def getHeight(self, node):
        '''Function that takes a node as imput and returns it's height'''
        if node == None:
            return 0
        else: 
            return node.height
    
    def getBalanceFactor(self, node):
        '''Function that determines the balance factor for a given node'''
        # check if empty node - return 0 if so
        if node == None: 
            return 0
        # node isn't empty (not leaf) - determine balance factor 
        # by taking the difference between height of left and right subtrees
        return self.getHeight(node.right) - self.getHeight(node.left)
    
    def recursivePut(self, node, key, value):
        '''Recursive function for inserting a node with key-value pair into AVLTreeMap 
        and adjusts the positions of nodes if the tree needs balancing'''
        # no node in the current position -- insert new node here
        if node == None:
            return WPINode(key, value)
        # key is less than the key of node in the current position - insert somewhere in the left subtree
        elif key < node.key:
            node.left = self.recursivePut(node.left, key, value)
        # key is greater than the key of node in the current position - insert somewhere in the right subtree
        elif key > node.key:
            node.right = self.recursivePut(node.right, key, value)
        # key is already a node in the tree - signifying an additional occurence of key in the webpage
        # append the index of this occurence of key to the already existing node's value attribute
        else: 
            node.value.append(value)
        
        # update height of ancestor node
        node.height = max(self.getHeight(node.left), self.getHeight(node.right)) + 1

        # get balance factor
        balanceFactor = self.getBalanceFactor(node)

        # rebalance if necessary
        # Left Left - right rotation to balance
        if balanceFactor < -1 and key < node.left.key:
            return self.rightRotate(node)
        # Right Right - left rotation to balance
        if balanceFactor > 1 and key > node.right.key:
            return self.leftRotate(node)
        # Left Right - left rotation followed by right rotation to balance
        if balanceFactor < -1 and key > node.left.key:
            node.left = self.leftRotate(node.left)
            return self.rightRotate(node)
        # Right Left - right roatation followed by left rotation to balance
        if balanceFactor > 1 and key < node.right.key:
            node.right = self.rightRotate(node.right)
            return self.leftRotate(node)
        
        # return balanced node
        return node

    def put(self, key, value):
        '''Wrapper for recursive function "recursivePut" - called to insert a new node into the tree'''
        # call recursivePut with root as starting node
        self.root = self.recursivePut(self.root, key, value)

    def leftRotate(self, x):
        ''' Performs Left Rotation of node and updates the height of effected nodes '''
        # save nodes into temp variables for swapping
        y = x.right
        z = y.left
        # perform left rotation 
        y.left = x
        x.right = z
        # update heights of nodes after adjustments to their positions are made
        x.height = max(self.getHeight(x.left), self.getHeight(x.right)) + 1
        y.height = max(self.getHeight(y.left), self.getHeight(y.right)) + 1

        return y

    def rightRotate(self, x):
        ''' Performs Right Rotation of node and updates the height of effected nodes'''
        # save nodes into temp variables for swapping
        y = x.left
        z = y.right
        # perform right rotation
        y.right = x
        x.left = z
        # update heights of nodes after adjustments to their positions are made
        x.height = max(self.getHeight(x.left), self.getHeight(x.right)) + 1
        y.height = max(self.getHeight(y.left), self.getHeight(y.right)) + 1

        return y


if __name__ == "__main__":
    path = "data\doc1-arraylist.txt"
    WPI = WebPageIndex(path)

    # Display AVL
    dt.print2D(WPI.root)

    # Test getCount 
    a = WPI.getCount("the") == 2
    b = WPI.getCount("a") == 2
    c = WPI.getCount("built-in") == 1
    d = WPI.getCount("modified") == 1
    e = WPI.getCount("remove") == 1
    f = WPI.getCount("create") == 1
    g = WPI.getCount("while") == 1
    h = WPI.getCount("ghd") == 0

    # prints True if successful, False otherwise
    test = a and b and c and d and e and f and g and h
    print(test)
