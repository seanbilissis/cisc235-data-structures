import DisplayTree as dt

class BSTNode:
    ''' Binary Search Tree Node'''
    def __init__(self, data = None):
        self.key = data
        self.left = None
        self.right = None
        self.height = 0


class BinarySearchTree:
    ''' Binary Search Tree object'''
    def __init__(self):
        self.root = None
    
    def sumHeights(self, node):
        '''Recursive function that compute the sum of height attributes or all nodes in the binary search tree '''
        if node == None:    # node doesnt exist and therefore no height to add
            return 0    
        else:               # node exists, take the sum of the nodes left subtree, itself, and its right subtree
            return self.sumHeights(node.left) + node.height + self.sumHeights(node.right)

    def getHeight(self, node):
        ''' Recursive function used to determine the value of a nodes height attribute'''
        if node == None or node.right == node.left: # node either doesnt exist or is a leaf, and therefore height will be zero
            return 0
        else:   # node exists and is not a leaf
            return max(self.getHeight(node.left), self.getHeight(node.right)) + 1   # Determine the height by adding 1 to either the height of its left or right subtree

    def recursiveInsert(self, node, x):
        '''Recursive function used to insert a node into the binary search tree '''
        # No node in the current position -- insert new node here with key x
        if node == None:
            return BSTNode(x)
        # x is less than the key of node in the current position -- insert somewhere in the left subtree
        elif x < node.key:
            node.left = self.recursiveInsert(node.left, x)
        # x is greater than the key of node in the current position -- insert somewhere in the right subtree
        elif x > node.key:
            node.right = self.recursiveInsert(node.right, x)
        # x is equal to the current node's key. Assuming no duplicates allowed, handles this case with an error message
        else:
            print("Error: Node key already in the tree --- Cannot insert duplicate")
            return node
        
        # determine the value for nodes height attribute and update
        node.height = self.getHeight(node)
        return node

    def insert(self, x):
        ''' Function called to insert a node with key x. Wrapper for recursive function "recursiveInsert" '''
        # call recursive function with the tree's root as the starting node
        self.root = self.recursiveInsert(self.root, x)
    
    def getTotalHeight(self):
        '''Function for obtaining the sum of the heights of the all nodes. Wrapper for recursive function sumHeights'''
        # call recursive function with the tree root as the starting node, then returns the result.
        return self.sumHeights(self.root)

""" def getWeightBalanceFactor(self, node): 
        '''Returns the weight balance factor of the tree. ***NOT WORKING****'''
        if node == None:    # node doesnt exist
            return 0    
        else:               # node exists
            return max(self.getWeightBalanceFactor(node.left), abs(node.left.height - node.right.height), self.getWeightBalanceFactor(node.right)) """


if __name__ == '__main__':
    # initialize tree
    Tree = BinarySearchTree()

    # values to test insertion
    values = [14, 10, 16, 5, 22, 21, 16, 11, 15, 7]
    # values = [10, 11, 12, 13, 14, 15, 15, 16, 17, 18]

    # insert values in list above
    for val in values: 
        Tree.insert(val)
    
    # display tree
    dt.print2D(Tree.root)

    # print True if correct total height
    x = Tree.getTotalHeight() == 9
    print(x)

    # test getWeightBalanceFactor ---- DOESNT WORK
    # y = Tree.getWeightBalanceFactor(Tree.root)
    # print(y)

