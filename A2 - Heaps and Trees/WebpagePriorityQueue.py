import WebPageIndex as WPI

class heapNode:
    '''WebpagePriorityQueue heap node'''
    def __init__(self, instance, value):
        self.instance = instance
        self.priority = value

class WebpagePriorityQueue:
    def __init__(self, wpi_list, query = None):
        self.query = query
        self.webpages = wpi_list
        self.heap = []
        self.size = 0
        # if initialized with a query string, call reheap function to generate maxheap
        if self.query != None:
            self.reheap(query)

    def peek(self):
        ''' Returns WebpageIndex with highest priority in the queue, but do not remove'''
        # the queue is empty - return None
        if self.size == 0:
            return None
        # queue is not empty - return the filename of the top node
        else:
            return self.heap[0]

    def poll(self):
        ''' Remove and return the WebpageIndex with highest priority in the queue '''
        # the queue is empty - return None
        if self.size == 0:
            return None
        # store top node (highest priority)
        highest = self.heap[0]
        # if only one node in queue, set heap to empty
        if self.size == 1: 
            self.heap = []
            self.size = 0
        # otherwise, copy right/bottom-most node to root, delete its old position, and reconfigure heap
        else:
            self.heap[0] = self.heap[-1]   # copy to root
            del self.heap[-1]               # remove old position
            self.size = len(self.heap)
            self.heapify(0)                 # configure heap
        
        # return highest priority node
        return highest

    def reheap(self, query):
        '''Function that takes a new query and reheaps the WebpagePriorityQueue'''
        self.query = query.lower()      # change query to all lowercase
        words = self.query.split()      # seperate query by whitespace to extract individual words into list
        words = [word.strip('.,;:!?()[]') for word in words]    # clean words of special characters/punctuation

        # iterate through webpageindex instances
        for instance in self.webpages:
            value = self.getPriority(words, instance)   # set the value property using getPriority function
            node = heapNode(instance, value)            # create new node representing the webpage
            self.heap.append(node)                      # add new node to heap
            self.size = len(self.heap)                  # change the size attribute

            # if more than one element in queue/heap, check if new node is correctly positioned
            if self.size > 1:
                parent = int((self.size -2)/2)  # index of the new nodes parent
                self.heapify(parent)            # reconfigure heap starting at the new nodes parent

    def heapify(self, idx):
        '''Function that rearranges the heap to satisfy maxheap properties'''
        left = 2*idx+1      # index of left child
        right = 2*idx+2     # index of right child
        x = idx             # index of current node - x will represent highest priority
        # if left child exists and has a higher priority, store it's index
        if left <= (self.size -1) and self.heap[left].priority > self.heap[x].priority:
            x = left
        # if right child exists and has a higher priority, store it's index
        if right <= (self.size -1) and self.heap[right].priority > self.heap[x].priority:
            x = right
        # if x is no longer the index of current node, then the heap needs rearranging
        if x != idx:
            self.heap[idx], self.heap[x] = self.heap[x], self.heap[idx] # swap current node with highest priority child
            self.heapify(int((idx-1)/2))    # check if further rearrangement needed after the above

    def getPriority(self, words, wpi):
        '''Function to determine the priority of a webpageidex instance by computing the 
        sum of the word counts on the webpage for all words in the query'''
        priority = 0
        # iterate through words in query, determine the number of occurences of that word, and add to priority
        for word in words:
            priority += wpi.getCount(word)  # get word count and add it to current priority value
        return priority




if __name__ == "__main__":
    # Some Testing Code
    """ t1 = WPI.WebPageIndex("CISC235\\15snb1-A2\A2_Extras\data\doc1-arraylist.txt")
        t2 = WPI.WebPageIndex("CISC235\\15snb1-A2\A2_Extras\data\doc2-graph.txt")
        t3 = WPI.WebPageIndex("CISC235\\15snb1-A2\A2_Extras\data\doc3-binarysearchtree.txt")
        t4 = WPI.WebPageIndex("CISC235\\15snb1-A2\A2_Extras\data\doc4-stack.txt")
        t5 = WPI.WebPageIndex("CISC235\\15snb1-A2\A2_Extras\data\doc5-queue.txt")
        t6 = WPI.WebPageIndex("CISC235\\15snb1-A2\A2_Extras\data\doc6-AVLtree.txt")

        listWPI = [t1, t2, t3, t4, t5, t6]
        query = "stack"

        WPQ = WebpagePriorityQueue(listWPI, query)

        print(WPQ.peek())
        i = 0
        for item in listWPI:
            i+=1
            c = item.getCount(query)
            print("Item: " + str(i) + "-------" + "Count: " + str(c))

        x = 0 
        while x < 6:
            z = WPQ.poll()
            print("Instance: " + str(z.instance.path) + "-------" + "Count: " + str(z.priority))
            x+=1 """
