""" 
Deliverable:    CISC235 Assignment 3
Student:        Sean Bilissis
ID:             20004468 
"""
import random
import math

class Graph():
    """ 
    Graph class that generates a random, connected, and undirected graph with n vertices
    OR 
    If random=False, a predetermined graph is generated for testing
    """
    def __init__(self, n, random=True):
        """ Initalization function for generating graph """
        self.matrix = [[0] * n for i in range(n)]   # initialize an n*n empty matrix
        if random: 
            self.random_graph_gen(n)    # generate a random graph with function random_graph_gen()
        else: 
            self.test_graph_gen()       # generate a predetermined graph with function test_graph_gen()
        # Print matrix for the graph (used for testing/verification)
        #print("Graph Matrix: \t", self.matrix)

    def random_graph_gen(self, n):
        """ Function for generating a random graph with n vertices """        
        for i in range(2, n+1):
            x = random.randint(1, i-1)      # x is a random integer in range [1 ... i-1]
            S = random.sample(range(1, i), x)   # let S be a randomly selected sample of x values from the set {1, ..., i-1}
            for s in S: 
                w = random.randint(10, 100)  # get weight of edge
                self.add_edge(i, s, w)       # add edge between i and s with weight w

    def test_graph_gen(self):
        """ Function that generates a predetermined graph """
        self.matrix = [[0, 15, 0, 7, 10, 0],
                    [15, 0, 9, 11, 0, 9],
                    [0, 9, 0, 0, 12, 7],
                    [7, 11, 0, 0, 8, 14],
                    [10, 0, 12, 8, 0, 8],
                    [0, 9, 7, 14, 8, 0]]

    def add_edge(self, v1, v2, w):
        """ 
        Function that adds an edge between vertices v1 and v2 with weight w
        by updating the corresponding cells in the matrix to the value w.
        """
        self.matrix[v1-1][v2-1] = w     # modify column for v2 in row v1
        self.matrix[v2-1][v1-1] = w     # modify column for v1 in row v2


    def sum_bfs(self):
        """ 
        Function implements a Breadth-First Search algorithm to create
        an instance of a spanning tree for the graph, and returns the sum of
        all edge weights in the spanning tree.
        """
        n = len(self.matrix)
        start = random.randint(1, n)    # get a random vertex to start from
        # Print starting vertex (for testing/verification purposes)
        # print("BFS starting at :  ", start)
        queue = []      # initialize a list to represent the queue
        added_to_queue = {key: False for key in range(1,n+1)}   # initialize a dictionary to track visited vertices
        queue.append(start)     # add the starting vertex to the queue  
        added_to_queue[start] = True    # indicate that start vertex has been visited
        sum_weights = 0     # initialize sum of weights
        while queue:        # while there are still vertices to process
            x = queue.pop(0)    # remove vertex at beginning of queue and store as x
            row = self.matrix[x-1]  # store the row associated with x in the graph
            for i in range(n):      # for each entry in the row (entries represent weights of edges)
                v = i+1         # adjust vertex reference as vertices begin from 1 and python indexes from 0
                if (row[i] > 0) and not(added_to_queue[v]):     # if there is a connection from x to v and v has not been visited
                    queue.append(v)         # add v to end of the queue
                    added_to_queue[v] = True    # indicate that v has been discovered
                    sum_weights += row[i]       # increment sum_weights by the weight of edge from x to v
                else:
                    continue
        return sum_weights


    def update_k(self, vertex):
        """ Function used to update the dictionary k. """
        row = self.matrix[vertex-1] # get matrix row associated with vertex
        for i in range(len(row)):   # iterate row entries
            if (row[i] != 0) and (row[i] < self.k[i+1][0]): # if entry is not zero and less than current k value for that vertex
                self.k[i+1] = (row[i], vertex)             # update k's value with new shortest distance
            else: 
                continue

    
    def prims_mst(self):
        """ 
        Function that grows a Minimum Spanning Tree using Prim's algorithm,
        and returns the sum of all edge weights in the MST.
        """
        n = len(self.matrix)
        self.k = {i: (math.inf, None) for i in range(1, n+1)}   # initialize k with value of inf and no parent
        S = set()   # initialize S for set of edges chosen by algorithm
        start = random.randint(1, n)    # get a random starting vertex
        T = set([start])        # initialize set T with starting vertex as the set of selected vertices
        self.update_k(start)    # update k with starting vertex
        Q = set([i for i in range(1, n+1) if i != start]) # initialize set Q with all vertices except our starting vertex

        sum_weights = 0
        while len(S) < n-1:     # while a MST has not been formed
            # min_edge = (vertex in Q, vertex in T, weight) such that weight is minimized
            min_edge = min([[q, self.k[q][1], self.k[q][0]] for q in Q], key=(lambda x: x[2]))
            to_v, from_v, weight = min_edge     # deconstruct values of min_edge to seperate variables
            sum_weights += weight   # increment sum_weights by weight of new edge
            e = (from_v, to_v)      # create tuple representing new edge
            S.add(e)        # add new edge to S
            T.add(to_v)     # add the previously unselected vertex to set T of selected
            self.update_k(to_v)  # update k with newly selected vertex
            Q.remove(to_v)       # remove newly selected vertex from set Q of unselected vertices
        # display path (used for testing/confirmation purposes):
        #print("MST Path: \n\t", S) 
        return sum_weights

def main():
    """ Main/Driver function that performs an experiment """
    k = int(input("Enter number of repetitions for each value of n:    "))    # get user input for experiment repetitions
    n_values = [20, 40, 60]    # values of n for the experiment
    for n in n_values:     # for each value of n
        DiffList = []      # initialize list of results obtained for current value of n
        while len(DiffList) < k:    # while list of results is less than the number of results/repetitions specified 
            G = Graph(n)        # initialize graph with n vertices
            B = G.sum_bfs()     # find sum of spanning tree using BFS
            P = G.prims_mst()   # find sum of minimal spanning tree using Prim's algorithm
            Diff = ((B - P)/P)*100  # compute percentage by which B is larger than P
            DiffList.append(Diff)   # add the result to the list of results for current value of n
        # After repeating experiment k times for current n : 
        avgDiff = sum(DiffList)/k   # Compute average percentage/value of results
        print("Average Value when n = ", n, " : ", '%.2f' % avgDiff, "%") # format and report the result


if __name__ == "__main__":
    # Flag to indicate whether to conduct experiment or run a test
    flag = True
    if flag:    # if flag == True
        main()  # call main to conduct experiment
    else:       # if flag == False
        # A flag to indicate if a random graph or predetermined graph is wanted for test
        random_graph = False
        if random_graph: # if random_graph == True
            n = 5           # set n to number of vertices for the graph
            G = Graph(5)    # create graph and store as G
        else:   # if random_graph == False
            G = Graph(0, random=False) # generate predetermined graph by setting random to False

        # Test BFS
        sum_weight_BFS = G.sum_bfs()
        print("BFS Sum: ", sum_weight_BFS)
        
        # Test Prims/MST
        sum_weight_MST = G.prims_mst()
        print("Prims/MST Sum: ", sum_weight_MST)
        




    
