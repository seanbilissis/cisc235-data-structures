''' CISC 235 - Assignment 1 (Question 4) - Implementing Stack with Array-Based Lists'''

class Stack:
    def __init__(self):
        self.stack = list()

    def isEmpty(self):
        if not self.stack:
            return True
        else:
            return False

    def push(self, x):
        self.stack.append(x)

    def pop(self):
        popped = self.stack.pop()
        print(popped) 

    def top(self):
        return self.stack[-1]

    def size(self):
        return len(self.stack)

if __name__ == "__main__":
    myStack = Stack()
    print("Is the stack empty?: " + str(myStack.isEmpty()))
    myStack.push("Data")
    myStack.push("More Data")
    myStack.push("Even More Data")
    print("Is the stack empty?: " + str(myStack.isEmpty()))
    print("How big is the stack?: " + str(myStack.size()))
    print("What is at the top of the stack?: " + str(myStack.top()))
    myStack.pop()
    print("What is at the top of the stack?: " + str(myStack.top()))