''' CISC 235 - Assignment 1 (Question 4) - Implementing Stack with Linked Lists'''

class Stack: 
    def __init__(self):
        self.head = None
        self.stackSize = 0

    def isEmpty(self):
        return (self.stackSize == 0)

    def push(self, e):
        newest = self.StackNode(e)
        newest.next = self.head
        self.head = newest
        self.stackSize += 1

    def pop(self):
        if self.head == None:
            print("ERROR: Cannot pop an empty stack")
            return
        else:
            popped = self.head.element
            self.head = self.head.next
            self.stackSize -= 1
        return popped

    def top(self):
        return self.head.element

    def size(self):
        return self.stackSize

    class StackNode:
        def __init__(self, e):
            self.next = None
            self.element = e

if __name__ == "__main__":
    myStack = Stack()
    print("Is the stack empty?: " + str(myStack.isEmpty()))
    myStack.push("Data")
    myStack.push("More Data")
    myStack.push("Even More Data")
    print("Is the stack still empty?: " + str(myStack.isEmpty()))
    print("How big is the stack?: " + str(myStack.size()))
    print("What is at the top of the stack?: " + str(myStack.top()))
    myStack.pop()
    print("What is at the top of the stack after popping?: " + str(myStack.top()))
    print("How big is the stack now?: " + str(myStack.size()))
    myStack.pop()
    myStack.pop()
    myStack.pop()