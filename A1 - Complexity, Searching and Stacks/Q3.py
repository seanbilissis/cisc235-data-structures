'''CISC325 - Assignment 1 (Question 3)'''
import random 
import time

def list_generator(size):
    ''' Accepts an integer arguement 'size' and generates a list of random even integers of length 'size' '''
    intList = []
    for i in range(size):
        intList.append(random.randrange(2, 1002, 2))     #Get a random even integer between 2 and 1000 (inclusive)
    return intList

def linear_search(listS, target):
    ''' Performs a linear search on the list passed into the function for target '''
    S = listS   #List to search through 
    x = target  #Value to search for
    for i in range(len(S)): #Loop through list and search
        if S[i] == x:
            return True #Return True if found
        else:
            continue
    return False #Return False if iteration completes

def binary_search(listS, target):
    ''' Binary Search Function '''
    low = 0                     #Low index
    high = len(listS)-1         #High index
    while high >= low:  
        mid = (high + low)//2   #Middle index
        if target == listS[mid]:
            return True         #Value at mid matches target
        if target < listS[mid]:
            high = mid - 1      #Target is less than value at mid - adjust high index to before mid
        else:
            low = mid + 1       #Target is greater than value at mid - adjust low index to after mid
    return False                #Target not found - return False

def partition(S, start, end):
    pivot = S[end]              #Set pivot to last index
    i = start                   #Set i to beginning index
    for j in range(i, end):
        if S[j] < pivot:        #Check if value at j is less than pivot
            S[j],S[i] = S[i],S[j]   #Swap to sort
            i += 1              #increment start position
    S[i],S[end] = S[end],S[i]   #Swap 
    return i

def q_sort(S, start, end):
    if start < end:            #Still need to partition further
        part = partition(S, start, end) #Partition lists
        q_sort(S, start, part - 1)  #Recusive call with first partition
        q_sort(S, part + 1, end)       #Recursive call with second partition


def algorithm_A(searchList, targetValues):
    ''' Performs a linear search on a given list for given target value '''
    searchIn = searchList   
    for target in targetValues:     #Linear search for each value in the target list
        isIn = linear_search(searchIn, target)


def algorithm_B(searchList, targetValues):
    ''' Sorts the passed-in list then performs a binary search for a target value '''
    searchIn = searchList
    q_sort(searchIn, 0, (len(searchIn) - 1))    #Sort the list
    for target in targetValues:                 #Binary search for each value in the target list
        isIn = binary_search(searchIn, target)

def main():
    results = []
    nList = [1000, 5000, 10000]         #List sizes
    for n in nList:
        S = list_generator(n)           #Call custom function to generate list of size n
        i = 1
        while i <= 100:
            kSize = 10*i                #Determine how many target values to search for based on iteration number
            kList = []                  #Empty list to fill with integers
            while len(kList) < kSize:
                k = random.randrange(1, kSize, 2)       #Random Odd Integer - Not in the list
                kList.extend([k, S[k]])                 #Grab value at index of the above integer in generated list - Ensures this target is in list

            startA = time.perf_counter()        #Start timer
            algorithm_A(S, kList)               #Run Algo A
            endA = time.perf_counter()          #End Timer
            
            startB = time.perf_counter()        #Start Timer
            algorithm_B(S, kList)               #Run Algo B
            endB = time.perf_counter()          #End Timer
            timeA = endA - startA               #Calculate time for A
            timeB = endB - startB               #Calculate time for B
            if (timeB < timeA):
                fasterWhen = (n, kSize)         
                results.append(fasterWhen)      #Return tuple when B is faster than A
                break
            else:
                i += 1                          #Continue the loop - A is still faster
    print(results)

if __name__ == "__main__":
    main()


